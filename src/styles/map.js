export default {
  totalHumaMachineCount: {
    width: 'max-content',
    marginLeft: '50%',
    transform: 'translateX(-50%)',
    fontFamily: 'Graphik Black',
    textAlign: 'center',
    marginTop: 10
  },
  first: {
    fontSize: 28,
    marginBottom: 10
  },
  second: {
    display: 'table',
    borderSpacing: 10
  },
  three: {
    fontSize: 28,
    marginTop: 10
  },
  tableCell: {
    display: 'table-cell',
    verticalAlign: 'middle'
  }
}