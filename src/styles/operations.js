export default {
  business: {
    dot: {
      display: 'table-cell',
      width: '6%',
      verticalAlign: 'middle',
      textAlign: 'right'
    },
    label: {
      display: 'table-cell',
      fontSize: 20,
      verticalAlign: 'middle',
      color: 'rgb(0,186, 255)',
      width: '45%',
      paddingLeft: 20,
      fontFamily: 'Graphik Medium'
    },
    value: {
      display: 'table-cell',
      fontSize: 30,
      verticalAlign: 'middle',
      width: '50%',
      fontFamily: 'Graphik Regular'
    },
    action: {
      display: 'table-cell',
      width: '2.5%',
      verticalAlign: 'middle',
      textAlign: 'right',
      paddingRight: 10
    }
  }
}