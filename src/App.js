import React from 'react';
import '../node_modules/font-awesome/css/font-awesome.min.css';
import './App.css';
import Clock from 'react-live-clock';
import Left from './components/Left';
import Center from './components/Center';
import { Wave } from 'react-animated-text';
import Right from './components/Right';
import FloatingButtons from './components/FloatingButtons'

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      fetchingData: false,
      operationView: false,
      masterDataTriggerValue: 'All',
      fabDisplay: 'hide',
      floatingButtonPosition: {
        x: 1135, y: 300
      },
      token: null
    }
    this.abortController = new AbortController(); // use to abort the fetch action when browser is close or change from customer view
    this.apiInterval = [];
    this.config = {};
    this.handleOverlap = this.handleOverlap.bind(this);
  }

  componentDidMount() {
    const self = this

    window.setAuthorization = (token) => self.setState({ token }, () => this.fetchAllData())

    window.masterDataTriggerValue = (masterDataTriggerValue) => {
      self.setState({ masterDataTriggerValue }, () => {
        self.fetchAllData(['masterDataByFilter'])
      }) 
    }
  }

  async fetchAllData(runModules = false) {
    const state = this.state
    const responseData = {};
    let configs = {};
    await fetch(`${process.env.PUBLIC_URL}/configs/config.json`).then(d => d.json()).then(data => {
      configs = data;
    })
    this.config = configs
    const urls = configs.urls;
    let modules = Object.keys(urls);
    let urlAppends = '';
    const masterDataTriggerValue = state.masterDataTriggerValue ? state.masterDataTriggerValue : 'All'

    if(runModules) {
      modules = modules.filter(module => runModules.includes(module))
      urlAppends = `?client=${masterDataTriggerValue}`
    }

    await Promise.all(
      modules.map(mod => {
        return fetch(`${urls[mod] + urlAppends}`,
          {
            signal: this.abortController.signal,
            headers: new Headers({
              'Authorization': `Bearer ${state.token}`
            })
          }
        )
      })
    ).then(response => Promise.all(response.map(el => el.json()))).then(data => {
      modules.map((mod, index) => responseData[mod] = data[index]);
    });

    if (responseData.masterDataFilter && responseData.masterDataFilter.message === '401 Unauthorized') return console.log(responseData.masterDataFilter.message)

    this.setState({ ...responseData, isLoading: true, config: this.config })
  }

  componentWillUnmount() {
    this.abortController.abort();
    this.apiInterval.map(notif => clearInterval(notif));
  }

  handleOverlap = (overlap) => {
    this.setState({ overlap })
  }

  launchToggleDrawer = (el) => {
    const classNameList = ['command-center-menu', 'command-center-panel right col-4 col-sm-1']
    const buttonPressClassName = el.target.className
    const isValidClass = classNameList.includes(buttonPressClassName)
    
    try {
      if (!isValidClass && buttonPressClassName.baseVal == undefined) return

      window.toggleDrawer()
    } catch (e) {
      console.log(e)
    }
  }

  handleButtonPress = (el) => {
    // for main display no floating button
    return

    const { fabDisplay } = this.state
    let { clientX, clientY } = el
    const classNameList = ['command-center-menu', 'command-center-panel right col-4 col-sm-1', 'menu-content']
    const buttonPressClassName = el.target.className
    const isValidClass = classNameList.includes(buttonPressClassName)

    if (el.touches) {
      clientX = el.touches[0].pageX
      clientY = el.touches[0].pageY
    }

    
    this.buttonPressTimer = setTimeout(() => {
      if (!isValidClass && buttonPressClassName.baseVal == undefined) return clearTimeout(this.buttonPressTimer)

      this.setState({ fabDisplay: fabDisplay == 'show' ? 'hide' : 'show', floatingButtonPosition: { x: clientX, y: clientY } }, () => clearTimeout(this.buttonPressTimer))
    }, 600)
  }

  handleButtonRelease = (el) => {
    clearTimeout(this.buttonPressTimer);
  }

  changeFabDisplayStatus = (ms = 1000) => {
    this.changeFabDisplayStatusTimer = setTimeout(() => {
      this.setState({ fabDisplay: 'hide' })
    }, ms)
  }

  changeFabDisplayStatusMouseEnter = () => {
    clearTimeout(this.changeFabDisplayStatusTimer)
  }

  render() {
    const { isLoading, masterDataByRegion, masterDataByFilter, masterDataFilter, clientGeo, clientServed, kpis, fetchingData, charts, cities, operationView, config, overlap, fabDisplay, floatingButtonPosition, token } = this.state;

    // loading screen when opening app
    if (!isLoading)
      return <div
        className="loading-screen"
        style={{
          // /backgroundImage: 'url(/ui-assets/images/maps/world.png)',
          height: 1080,
          backgroundPosition: 'center',
          backgroundRepeat: 'no-repeat',
          backgroundSize: 'cover',
          fontFamily: 'Graphik Black'
        }}>
        <div style={{
          position: 'absolute',
          width: 'max-content',
          top: '50%',
          left: '50%',
          transform: 'translate(-50%, -50%)',
          fontSize: '90pt',
          color: "#00BAFF",
          mixBlendMode: 'screen',
        }}>
          <Wave text="DOWNLOADING RESOURCES" effect="jump" speed={20} effectChange={1.5} />
        </div>
      </div>
    return (
      <div className="App" onDoubleClick={(el) => this.launchToggleDrawer(el) }
        onTouchStart={(el) => this.handleButtonPress(el)} onTouchEnd={(el) => this.handleButtonRelease(el)}
        onMouseDown={(el) => this.handleButtonPress(el)} onMouseUp={(el) => this.handleButtonRelease(el)}
        onMouseLeave={(el) => this.handleButtonRelease(el)} >

        <FloatingButtons fabDisplay={fabDisplay} imgSrc={`${process.env.PUBLIC_URL}/ui-assets/images/SynOps-logo.png`}
          floatingButtonPosition={floatingButtonPosition}
          changeFabDisplayStatus={() => this.changeFabDisplayStatus()}
          changeFabDisplayStatusWithTimer={() => this.changeFabDisplayStatus(5000)}
          changeFabDisplayStatusMouseEnter={() => this.changeFabDisplayStatusMouseEnter()} />

        <div id="particles-js" style={{ width: '100%', height: '1080', position: 'absolute', top: 0, left: 0, zIndex: -1 }}></div>
        {!overlap && <img src={`${process.env.PUBLIC_URL}/ui-assets/images/SynOps-logo.png`} style={{ position: 'absolute', left: '16%', top: '50%', transform: 'translateY(-40%)' }} />}
        {!overlap && <div style={{ position: 'absolute', left: '30pt', bottom: 20, fontSize: '1.5rem', color: '#000' }}>
          <img src={`${process.env.PUBLIC_URL}/ui-assets/images/SynOps-logo.png`} style={{ width: '14%', float: 'left' }} />
          &nbsp;&nbsp;&nbsp;Powered <br />
          &nbsp;&nbsp;&nbsp;by <span style={{ color: '#00bbff' }}>SynOps</span>
        </div>}
        <div className="command-center-timestamp">
          {/* , marginLeft: overlap ? '520px' : '0px' */}
          {!overlap && <div style={{ fontSize: 70, marginTop: '50pt' }}>
            SynOps Hub
          </div>}
        </div>
        <Left charts={charts.data} config={this.config} onOverLap={this.handleOverlap} operationView={operationView} hideFabDisplayStatus={() => this.changeFabDisplayStatus(100)}/>
        <Center hideFabDisplayStatus={() => this.changeFabDisplayStatus(100)} data={masterDataByRegion.data} deployments={masterDataByFilter.data} filters={masterDataFilter.data} clientGeo={clientGeo} fetchingData={fetchingData} cities={cities} clientServed={clientServed} config={this.config} operationView={operationView} />
        <Right kpis={kpis} config={this.config} operationView={operationView} hideFabDisplayStatus={() => this.changeFabDisplayStatus(100)} token={token} />
        <div id="operation-view-button"
          style={{ position: 'absolute', bottom: 10, left: 10, backgroundColor: '#00B4FF', width: 'max-content', padding: '1rem', display: 'none' }}
          onClick={() => {
            if (config && !config.debug) {
              try {
                window.dashboard.hide()
                window.dashboard.hideOperationsDashboard();
              } catch (e) {
                console.log(e)
              }
            }
            this.setState({ operationView: true, overlap: false })
          }}
        >OperationView</div>
        <div id="customer-view-button"
          style={{ position: 'absolute', bottom: 10, left: 10, backgroundColor: '#00B4FF', width: 'max-content', padding: '1rem', display: 'none' }}
          onClick={() => {
            this.setState({ operationView: false, overlap: false });
          }}
        >CustomerView</div>
      </div>
    );
  }
}

export default App;