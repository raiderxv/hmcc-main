import React, { Component } from 'react';
import '../styles/left/main.css';
import '../styles/left/submenu.css';
import '../styles/left/charts.css';

const textOverlayStyles = {
  maxWidth: '60%',
  position: 'absolute',
  left: '50%',
  top: '50%',
  transform: 'translate(-50%, -50%)',
  textAlign: 'center',
  mixBlendMode: 'difference',
  color: '#00BAFF',
  fontSize: '1.4rem',
  fontFamily: 'Graphik Black'
}

class Left extends Component {
  constructor(props) {
    super(props);
    this.state = {
      menuClick: false,
      posMenuContent: { top: 62, left: 40, paddingTop: 92 },
      menus: this.props.charts,
      menuDetails: [],
      overview: false,
      subMenuActive: false,
      chartActive: false
    }
    this.handleMenuClick = this.handleMenuClick.bind(this);
    this.handleSubMenuClick = this.handleSubMenuClick.bind(this);
    this.abortController = new AbortController();
    this.handleChartMouseDown = this.handleChartMouseDown.bind(this);
    this.handleChartMouseMove = this.handleChartMouseMove.bind(this);
    this.handleChartMouseUp = this.handleChartMouseUp.bind(this);
  }

  handleMenuClick(e) {
    const { menuClick, menus, chartActive } = this.state;

    if (chartActive) return;

    const target = e.target;
    const clickedMenu = target.dataset.id;
    const isSameClick = menuClick === clickedMenu;
    this.props.onOverLap(!isSameClick);
    this.setState({ menuClick: isSameClick ? false : clickedMenu, chartActive: false, subMenuActive: false, menuDetails: [] }, () => {
      if (!isSameClick) {

        const menuPosition = target.getBoundingClientRect();
        const menuDetails = menus.filter(menu => menu.code === clickedMenu)[0];

        const posMenuContent = { top: menuPosition.top, left: menuPosition.left, paddingTop: menuPosition.height };
        console.log(menuDetails)
        this.setState({ posMenuContent, menuDetails, subMenuActive: menuDetails.children.length ? menuDetails.children[0].code : '' })
      }
    })
  }
  handleSubMenuClick(e) {
    const target = e.target;
    const subMenuActive = target.dataset.id;
    if (this.state.subMenuActive === subMenuActive)
      return;
    this.setState({ subMenuActive })
  }
  handleChartClick = (e) => {
    let target = e.target;
    if (target.getAttribute('class') === 'text-overlay') {
      target = target.parentNode
    }

    let stateProps = { chartActive: target.dataset.src };
    if (target.dataset.id === 'OPERATIONSDASHBOARD') {
      stateProps['menuClick'] = 'OPERATIONSDASHBOARD'
    }
    this.setState(stateProps, () => {
      if (!this.props.config.debug)
        setTimeout(() => {
          try {
            console.log(stateProps.chartActive)
            const dashboard = document.getElementById('dashboard-fs').getBoundingClientRect();
            const resolution = { w: 1632, h: 918 }
            window.dashboard.show(resolution.w, resolution.h, parseInt(dashboard.x + 5), parseInt(dashboard.y + 92 + 5));
            // window.dashboard.show(1920, 1080, 0, 0);
            console.log(stateProps.chartActive)
            window.dashboard.loadById(parseInt(stateProps.chartActive));
          } catch (e) {
            console.log(e);
          }
        }, 100)
    })
  }
  handleChartMouseDown = (e) => {
    this.setState({ drag: true, chartScrollY: e.pageY })
  }
  handleChartMouseUp(e) {
    this.setState({ drag: false, chartScrollY: 0 })
  }
  handleChartMouseMove = (e) => {
    if (!this.state.drag)
      return
    const charts = document.getElementsByClassName('charts')[0];
    charts.scrollTop = charts.scrollTop + (this.state.chartScrollY - e.pageY);
  }
  render() {
    const { menuClick, posMenuContent, menus, menuDetails = [], subMenuActive, chartActive } = this.state;
    const charts = menuDetails.children && menuDetails.children.length ? menuDetails.children.filter(menu => menu.code === subMenuActive)[0].children : [];
    return (
      <div className="command-center-panel col-sm-1 col-4" style={{ display: `${this.props.operationView ? 'none' : 'inline-flex'}` }} onClick={() => this.props.hideFabDisplayStatus()} >
        <div className="command-center-menu">
          <ul id="main-menu" className="main-menu" style={{ marginTop: menuClick ? (menuClick === 'OPERATIONSDASHBOARD' ? '8rem' : '7rem') : '35%', marginLeft: '30pt' }} >
            {menus.map(menu => {
              if (!menu.show_on_menu)
                return;

              if (!menuClick || (menuClick && menuClick === menu.code))
                return <li
                  key={menu.id}
                  className="menu"
                  data-id={menu.code}
                  data-src={menu.id}
                  onClick={menu.code === 'OPERATIONSDASHBOARD' ? this.handleChartClick : this.handleMenuClick}
                  style={{
                    display: `${menuClick ? (menuClick === menu.code ? 'block' : 'none') : 'block'}`,
                    backgroundImage: `url(${process.env.PUBLIC_URL}/ui-assets/images/buttons/charts/${menu.code}_${menuClick === menu.code ? "ON" : "OFF"}.png)`,
                    backgroundColor: `#${menuClick === menu.code ? "004dff" : "00BAFF"}`
                  }}
                >
                  {menu.description}
                </li>
            })}
          </ul>
        </div>
        {menuClick && <div className="menu-content" style={{ ...posMenuContent, zIndex: 10, height: 800 + posMenuContent.paddingTop, width: 1650 }}>
          {menuClick !== 'OPERATIONSDASHBOARD' && <h2>OVERVIEW DASHBOARD</h2>}
          {menuClick !== 'OPERATIONSDASHBOARD' && <div className="overview" style={{
            position: 'relative',
            width: 570,
            height: 350,
            backgroundImage: `url(${this.props.config.imgBaseUrl}${menuDetails.thumbnail_path})`,
            backgroundColor: '#00BAFF',
            backgroundSize: 'cover'
          }}
            data-src={menuDetails.id}
            onClick={this.handleChartClick}
          >
            <div className="text-overlay" style={textOverlayStyles}>{menuDetails.description}</div>
          </div>
          }

          {menuClick !== 'OPERATIONSDASHBOARD' && <div className="submenu">
            {menuDetails.children && menuDetails.children.map(menu => {
              return <div key={menu.id} className={`item${subMenuActive === menu.code ? ' active' : ''}`} data-id={menu.code} onClick={this.handleSubMenuClick} >{menu.description}</div>
            })}
          </div>
          }
          <div className="clear-div" style={{ clear: 'both' }}></div>
          {menuClick !== 'OPERATIONSDASHBOARD' && <div className="charts" onMouseDown={this.handleChartMouseDown} onMouseMove={this.handleChartMouseMove} onMouseUp={this.handleChartMouseUp}>
            {charts && charts.map((chart, index) => {
              return <div
                key={index}
                className="charts-overlay"
                style={{
                  position: 'relative',
                  // width: 500,
                  width: 570,
                  height: 250,
                  overflow: 'hidden',
                  marginRight: '0.5rem',
                  marginBottom: '0.5rem',
                  backgroundColor: '#00BAFF',
                  backgroundImage: `url(${this.props.config.imgBaseUrl}${chart.thumbnail_path})`,
                  backgroundSize: 'cover'
                }}
                data-src={chart.id} onClick={this.handleChartClick}
              >
                <div className="text-overlay" style={textOverlayStyles}>{chart.description}</div>
              </div>
            })}
            <div className="clear-div" style={{ clear: 'both' }}></div>
          </div>
          }
          {chartActive && <div style={{ position: 'absolute', width: 1632, height: 918, left: 0, top: 0, padding: `${posMenuContent.paddingTop + 5}px 5px 5px 5px`, backgroundColor: '#001F73', zIndex: 100 }} id="dashboard-fs">
            {/* <img src="https://rog.asus.com/media/s/1539961014547.png" style={{ width: 1632, height: 918 }} /> */}
            <img id="left-dashboard-refresh" alt="left-dashboard-refresh" src={`${process.env.PUBLIC_URL}/ui-assets/images/buttons/refresh.png`} style={{ position: 'absolute', top: 37, right: 70, width: 50, height: 50, backgroundColor: '#FFF' }}
              onClick={(e) => !this.props.config.debug && window.dashboard.refreshByID(parseInt(chartActive))} />
            <img id="left-dashboard-close" alt="close-dashboard-button" src={`${process.env.PUBLIC_URL}/ui-assets/images/buttons/closebutton.png`} style={{ position: 'absolute', top: 37, right: 5, width: 50, height: 50 }}
              onClick={(e) => this.setState({ chartActive: false, menuClick: this.state.menuClick === 'OPERATIONSDASHBOARD' ? false : this.state.menuClick }, () => {
                try {
                  if (!this.props.config.debug)
                    window.dashboard.hide()
                } catch (e) {
                  console.log(e);
                }
              })} />
          </div>}
        </div>}
      </div>
    );
  }
}

export default Left;