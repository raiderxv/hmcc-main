import React, { Component } from 'react';
import { Xymon, Snow, GoLive } from './notifications';
import '../styles/right/left.css';
import '../styles/right/notification.css';
import { Bounce, Fade, Slide, Rotate, Zoom, Roll, LightSpeed } from 'react-reveal';
import Scroll from 'react-scroll';

import OperationStyles from '../styles/operations';

const numeral = require('numeral');

const tickerStyles = {
  width: 20,
  height: 20
}

const operationStyles = {
  clientServed: {
    display: 'table-cell',
    width: 170,
    height: 50,
    textAlign: 'center',
    fontSize: '1.7rem',
    verticalAlign: 'middle',
    borderRadius: '10px'
  },
  title: {
    fontSize: '1.5rem',
    padding: '0rem 0rem'
  }
}

class Right extends Component {
  constructor(props) {
    super(props);
    this.kpiKeys = {};
    for (const i in this.props.kpis.data) {
      this.kpiKeys[i] = 0;
    }
    this.state = {
      ...this.kpiKeys,
      notifications: [],
      startNotifIndex: 0,
      startAnnouncementIndex: 0,
      kpiImage: false,
      animations: [],
      AnimationAnnouncement: false,
      announcementIndex: 0,
      announcements: []
    }
    this.counters = [];
    this.notificationInterval = false;
    this.notificationIndexInterval = false;
    this.announcementIndexInterval = false;
    this.scroll = Scroll.scroller;
    this.handleOperationView = this.handleOperationView.bind(this);
  }
  // this.props.notifications.data[0].notifications.map(n => this.handleRandomAnimation())
  fetchNotifications() {
    if (this.notificationIndexInterval) clearInterval(this.notificationIndexInterval);
    if (this.notificationInterval) clearInterval(this.notificationInterval)
    fetch(`${this.props.config.fetchAfterPageLoad.notifications}`, {
      headers: new Headers({ 'Authorization': `Bearer ${this.props.token}` })
    }).then(n => n.json()).then(response => {
      // const data = response.data;
      // console.log(response)
      try {
        const data = response.data;
        // const exceedP1 = data.filter(d => {
        //   return d.Severity === 'P1' //&& d.SLA_Status.toLowerCase().includes('exceeded');
        // }).sort((a, b) => (new Date(b.Incident_Start_Date_GMT) - new Date(a.Incident_Start_Date_GMT)))
        // const withinP1 = data.filter(d => {
        //   return d.Severity === 'P1' //&& d.SLA_Status.toLowerCase().includes('within');
        // }).sort((a, b) => (new Date(b.Incident_Start_Date_GMT) - new Date(a.Incident_Start_Date_GMT)))

        // const exceedP2 = data.filter(d => {
        //   return d.Severity === 'P2' //&& d.SLA_Status.toLowerCase().includes('exceeded');
        // }).sort((a, b) => (new Date(b.Incident_Start_Date_GMT) - new Date(a.Incident_Start_Date_GMT)))
        // const withinP2 = data.filter(d => {
        //   return d.Severity === 'P2' //&& d.SLA_Status.toLowerCase().includes('within');
        // }).sort((a, b) => (new Date(b.Incident_Start_Date_GMT) - new Date(a.Incident_Start_Date_GMT)))

        const notifications = [...data];
        const formattedNotifications = [];
        let notifs = [];
        notifications.map((notif, n) => {
          notifs.push(notif);
          if ((n + 1) % 2 === 0 || n === notifications.length - 1) {
            formattedNotifications.push(notifs)
            notifs = [];
          }
        })

        this.setState({ notifications: formattedNotifications, startNotifIndex: 0, animations: formattedNotifications[0].map(n => this.handleRandomAnimation()), elapse: response.elapse_per_page * 1000 }, () => {

          this.notificationIndexInterval = setInterval(() => {
            const index = this.state.startNotifIndex === this.state.notifications.length - 1 ? 0 : this.state.startNotifIndex + 1;
            const animations = formattedNotifications[index].map(n => this.handleRandomAnimation())
            this.setState({ startNotifIndex: index, animations })
          }, response.elapse_per_page * 1000);
        })
      } catch (e) {
        this.setState({ notifications: [], startNotifIndex: 0 })
      }
      this.notificationInterval = setInterval(() => this.fetchNotifications(), 900000);
    })
  }

  fetchAnnouncement() {
    fetch(`${this.props.config.fetchAfterPageLoad.announcements}`, {
      headers: new Headers({ 'Authorization': `Bearer ${this.props.token}` })
    }).then(a => a.json()).then(d => {
      try {
        this.setState({ announcements: d.data, AnimationAnnouncement: this.handleRandomAnimation(), announcementIndex: 0 }, () => {

          this.announcementIndexInterval = setInterval(() => {
            if (this.state.announcementIndex === this.state.announcements.length - 1) {
              clearInterval(this.announcementIndexInterval);
              this.fetchAnnouncement();
              return;
            }
            this.setState({ announcementIndex: this.state.announcementIndex + 1, AnimationAnnouncement: this.handleRandomAnimation() })
          }, 15 * 1000)

          // this.fetchAnnouncement();
        })
      } catch (e) {
        setTimeout(() => this.fetchAnnouncement(), 200000);
      }
    })
  }

  handleOperationView = (e) => {
    if (!this.props.config.debug) {
      const operationDashboard = document.getElementById('operations-dashboard');
      setTimeout(() => {
        try {
          const rect = operationDashboard.getBoundingClientRect();
          // console.log('sdfasdf', parseInt(rect.width), parseInt(rect.height), parseInt(rect.x), parseInt(rect.y))
          // NOTE: showOperationsDashboard is for launcher
          window.dashboard.showOperationsDashboard(parseInt(rect.width), parseInt(rect.height), parseInt(rect.x), parseInt(rect.y))
          // window.dashboard.showOperationsDashboard(parseInt(rect.width), parseInt(rect.height), 0, 400)
        } catch (e) {
          console.log(e)
        }
      }, 200)
    }
  }

  handleRandomAnimation() {
    const animations = [Bounce, Fade, Slide, Rotate, Zoom, Roll, LightSpeed];
    const rand = Math.random();
    let randomNumber = Math.floor(rand * (animations.length - 1));
    if (randomNumber >= animations.length)
      randomNumber = animations.length - 1;

    return animations[randomNumber];
  }

  handleComputationClick = (e) => {
    const target = e.target;
    const id = target.dataset.id;
    const kpiImage = {
      src: this.props.kpis.data[id].Image,
      name: id
    }
    this.setState({ kpiImage })
  }
  handleComputationClose = () => {
    this.setState({ kpiImage: false })
  }

  componentDidMount() {
    const { data } = this.props.kpis;
    for (const i in data) {
      const d = data[i];
      const seconds = (new Date() - new Date(d.GoLive_Date)) / 1000;
      const metric_data = i === 'DigiTracker' ? d.KPI_Value : d.KPI_Value + (d.KPI_Delta_Per_Second * seconds);
      // const metric_data = i === 'DigiTracker' ? 360000000 : d.KPI_Value + (d.KPI_Delta_Per_Second * seconds);
      this.setState({ [i]: metric_data }, () => {

        if (['NetValue', 'TotalValue', 'HoursSaved', 'DiverseDataElements'].includes(i)) {
          const timeInterval = setInterval(() => {
            this.setState({ [i]: this.state[i] += d.KPI_Delta_Per_Second });
          }, 1000)
          this.counters.push(timeInterval)
        }

        if (['RolesTransformed', 'ExpertsAugmented'].includes(i)) {
          const timeInterval = setInterval(() => {
            this.setState({ [i]: this.state[i] += d.KPI_Delta });
          }, 100)
          this.counters.push(timeInterval)
        }

        if ('DigiTracker' === i) {
          const timeInterval = setInterval(() => {
            this.setState({ [i]: this.state[i] += d.KPI_Delta_Per_Second });
            // this.setState({ [i]: this.state[i] += 2.31 });
          }, 1000)
          this.counters.push(timeInterval);
        }
      });
    }
    // this.notificationIndexInterval = setInterval(() => {
    //   const animations = this.props.notifications.data[0].notifications.map(n => this.handleRandomAnimation())
    //   const index = this.state.startNotifIndex < this.state.notifications.length - 1 ? this.state.startNotifIndex + 1 : 0;
    //   this.setState({ startNotifIndex: index, animations })
    // }, this.props.notifications.elapse_per_page * 1000);
    this.fetchNotifications();
    this.fetchAnnouncement();
  }

  componentWillUnmount() {
    Scroll.Events.scrollEvent.remove('begin');
    Scroll.Events.scrollEvent.remove('end');
    this.counters.map(c => clearInterval(c))
    clearInterval(this.notificationIndexInterval);
  }

  render() {
    const { NetValue, TotalValue, DigiTracker, DiverseDataElements, ExpertsAugmented, HoursSaved, RolesTransformed, notifications, startNotifIndex, kpiImage, announcements, announcementIndex, AnimationAnnouncement } = this.state;
    // const activeNotifications = notifications[-1];
    // const activeNotifications = notifications[0];
    const activeNotifications = notifications[startNotifIndex];
    const activeAnnouncement = announcements[announcementIndex];
    const operationViewStyles = {
      width: 750,
      float: 'right',
      display: 'unset'
    }
    return (
      <div className="command-center-panel right col-4 col-sm-1" style={this.props.operationView ? operationViewStyles : {}} onClick={() => this.props.hideFabDisplayStatus()} >
        <div className="live-data" style={this.props.operationView ? { position: 'unset' } : {}}>
          <div className="business-outcomes">
            <div className="label" style={operationStyles.title}>
              <img alt="business outcomes" src={`${process.env.PUBLIC_URL}/ui-assets/images/operations/business-outcomes.png`} style={{ width: 20 }}></img>
              &nbsp;&nbsp;BUSINESS OUTCOMES
            </div>
            <div style={{ display: 'table', width: '100%', borderSpacing: '0 5px' }}>
              <div style={{ display: 'table-row', height: 55, backgroundColor: 'rgba(0,0,0,0.5)' }}>
                <div style={OperationStyles.business.dot}>
                  <img alt="NetValue" src={`${process.env.PUBLIC_URL}/ui-assets/images/dot.gif`} style={{ height: 20, width: 20 }} />
                </div>
                <div style={OperationStyles.business.label}>Net Value P&L</div>
                <div style={OperationStyles.business.value}>{numeral(NetValue).format('$0,0')}</div>
                <div style={OperationStyles.business.action}><img className="operations-arrow-img" alt="net-value-arrow" src={`${process.env.PUBLIC_URL}/ui-assets/images/operations/trigger.png`} data-id="NetValue" onClick={this.handleComputationClick} /></div>
              </div>

              <div style={{ display: 'table-row', height: 55, backgroundColor: 'rgba(0,0,0,0.5)' }}>
                <div style={OperationStyles.business.dot}>
                  <img alt="TotalValue" src={`${process.env.PUBLIC_URL}/ui-assets/images/dot.gif`} style={{ height: 20, width: 20 }} />
                </div>
                <div style={OperationStyles.business.label}>Total Value</div>
                <div style={OperationStyles.business.value}>{numeral(TotalValue).format('$0,0')}</div>
                <div style={OperationStyles.business.action}><img className="operations-arrow-img" alt="net-value-arrow" src={`${process.env.PUBLIC_URL}/ui-assets/images/operations/trigger.png`} data-id="TotalValue" onClick={this.handleComputationClick} /></div>
              </div>

              <div style={{ display: 'table-row', height: 55, backgroundColor: 'rgba(0,0,0,0.5)' }}>
                <div style={OperationStyles.business.dot}>
                  <img alt="HoursSaved" src={`${process.env.PUBLIC_URL}/ui-assets/images/dot.gif`} style={{ height: 20, width: 20 }} />
                </div>
                <div style={OperationStyles.business.label}>Hours Saved</div>
                <div style={OperationStyles.business.value}>{numeral(HoursSaved).format('0,0')}</div>
                <div style={OperationStyles.business.action}><img className="operations-arrow-img" alt="net-value-arrow" src={`${process.env.PUBLIC_URL}/ui-assets/images/operations/trigger.png`} data-id="HoursSaved" onClick={this.handleComputationClick} /></div>
              </div>

              <div style={{ display: 'table-row', height: 55, backgroundColor: 'rgba(0,0,0,0.5)' }}>
                <div style={OperationStyles.business.dot}>
                  <img alt="RolesTransformed" src={`${process.env.PUBLIC_URL}/ui-assets/images/dot.gif`} style={{ height: 20, width: 20 }} />
                </div>
                <div style={OperationStyles.business.label}>Roles Transformed</div>
                <div style={OperationStyles.business.value}>{numeral(RolesTransformed).format('0,0')}</div>
                <div style={OperationStyles.business.action}><img className="operations-arrow-img" alt="net-value-arrow" src={`${process.env.PUBLIC_URL}/ui-assets/images/operations/trigger.png`} data-id="RolesTransformed" onClick={this.handleComputationClick} /></div>
              </div>
            </div>
          </div>
          <div className="powered-by">
            <div className="label" style={operationStyles.title}>
              <img alt="poweredby" src={`${process.env.PUBLIC_URL}/ui-assets/images/operations/powered-by.png`} style={{ width: 20 }}></img>
              &nbsp;&nbsp;POWERED BY
            </div>
            <div style={{ display: 'table', width: '100%', borderSpacing: '0 5px' }}>
              <div style={{ display: 'table-row', height: 55, backgroundColor: 'rgba(0,0,0,0.5)' }}>
                <div style={OperationStyles.business.dot}>
                  <img alt="DiverseDataElements" src={`${process.env.PUBLIC_URL}/ui-assets/images/dot.gif`} style={{ height: 20, width: 20 }} />
                </div>
                <div style={OperationStyles.business.label}>Diverse Data Elements</div>
                <div style={OperationStyles.business.value}>{numeral(DiverseDataElements).format('0,0')}</div>
                <div style={OperationStyles.business.action}><img className="operations-arrow-img" alt="net-value-arrow" src={`${process.env.PUBLIC_URL}/ui-assets/images/operations/trigger.png`} data-id="DiverseDataElements" onClick={this.handleComputationClick} /></div>
              </div>
              <div style={{ display: 'table-row', height: 55, backgroundColor: 'rgba(0,0,0,0.5)' }}>
                <div style={OperationStyles.business.dot}>
                  <img alt="DigiTracker" src={`${process.env.PUBLIC_URL}/ui-assets/images/dot.gif`} style={{ height: 20, width: 20 }} />
                </div>
                <div style={OperationStyles.business.label}>Digital Transactions</div>
                <div style={OperationStyles.business.value}>{numeral(DigiTracker).format('0,0')}</div>
                <div style={OperationStyles.business.action}><img className="operations-arrow-img" alt="net-value-arrow" src={`${process.env.PUBLIC_URL}/ui-assets/images/operations/trigger.png`} data-id="DigiTracker" onClick={this.handleComputationClick} /></div>
              </div>
              <div style={{ display: 'table-row', height: 55, backgroundColor: 'rgba(0,0,0,0.5)' }}>
                <div style={OperationStyles.business.dot}>
                  <img alt="ExpertsAugmented" src={`${process.env.PUBLIC_URL}/ui-assets/images/dot.gif`} style={{ height: 20, width: 20 }} />
                </div>
                <div style={OperationStyles.business.label}>Specialized Talent</div>
                <div style={OperationStyles.business.value}>{numeral(ExpertsAugmented).format('0,0')}</div>
                <div style={OperationStyles.business.action}><img className="operations-arrow-img" alt="net-value-arrow" src={`${process.env.PUBLIC_URL}/ui-assets/images/operations/trigger.png`} data-id="ExpertsAugmented" onClick={this.handleComputationClick} /></div>
              </div>
            </div>
          </div>

        </div >
        <div className="live-notification" style={this.props.operationView ? { position: 'unset' } : {}}>
          <div className="label" style={{
            fontSize: '1.5rem',
            padding: '0rem 0rem'
          }}>
            <img alt="live-notifications" src={`${process.env.PUBLIC_URL}/ui-assets/images/operations/live-notif.png`} style={{ width: 20 }} />
            &nbsp;&nbsp;LIVE NOTIFICATIONS
          </div>
          <div className="notification" style={{ marginTop: 3.5 }}>
            {/* animation if the notifications is fething data */}
            {activeAnnouncement && <AnimationAnnouncement children={<GoLive data={{...activeAnnouncement, config: this.props.config}} />} />}
            {!activeNotifications && <div className="zero-notif" style={{ backgroundColor: 'rgba(0,0,0,0.5)', height: 150 }}>
              <div style={{ position: 'relative', top: '50%', left: '50%', transform: 'translate(-50%, -50%)', width: 'max-content', fontSize: '2rem', textTransform: 'uppercase' }}>No Open Tickets</div>
            </div>}
            {activeNotifications && activeNotifications.map((n, i) => {
              const Animation = this.state.animations[i];
              return <Animation left key={`${i}-${startNotifIndex}`} children={<Snow data={n} />} />

              // return <div><Snow data={n} /></div>
              // if (n.type === 'xymon')  
              //   return <div>< Xymon data={n} /></div>
              // if (n.type === 'snow')
              //   return <div><Snow data={n} /></div>
              // return <div><GoLive data={n} /></div>
            })}
          </div>
        </div>
        {!this.props.operationView && <div style={{ position: 'absolute', top: '47%', left: '25%', width: '73%', height: '51%' }} onLoad={this.handleOperationView}>
          <div style={{ ...operationStyles.title, padding: '0.6rem 0rem' }}>
            <img alt="operations-dashbords" src={`${process.env.PUBLIC_URL}/ui-assets/images/operations/dashboard.png`} style={{ width: 20 }}></img>
            &nbsp;&nbsp;OPERATIONS DASHBOARD
            </div>
          <div id="operations-dashboard" style={{
            width: 1400, height: '90%', backgroundColor: '#00B4FF'
          }}></div>
        </div>}
        {
          kpiImage && <div className="kpi-container">
            <img className="close" src={`${process.env.PUBLIC_URL}/ui-assets/images/buttons/closebutton.png`} onClick={this.handleComputationClose} alt="close-dashboard-img" />
            {/* <div className="kpi-name">{kpiImage.name}</div> */}
            <img className="source" src={`data:image/png;base64, ${kpiImage.src}`} alt="close-source-img" />
          </div>
        }
      </div >
    );
  }
}

export default Right;