import React, { Component } from 'react';

class Xymon extends Component {
  render() {
    const { Status, ServerName, Process1, Process2, IPAddress } = this.props.data;
    return (
      <div className="xymon">
        <div></div>
        <div>
          <div className="status" style={{ backgroundColor: Status }}>&nbsp;</div>
          <div className="server-name">{ServerName}</div>
          <div className="data-row">Process:&nbsp;&nbsp;{Process1} / {Process2}</div>
          <div className="data-row">IP ADD:&nbsp;&nbsp;{IPAddress}</div>
        </div >
      </div >
    );
  }
}

export default Xymon;