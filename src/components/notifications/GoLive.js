import React, { Component } from 'react';

class GoLive extends Component {
  render() {
    let { thumbnail, title, body, config } = this.props.data;
    thumbnail = config.imgBaseUrl+ thumbnail;

    return (
      <div className="go-live" style={{ backgroundColor: 'rgba(0,0,0,0.5)', padding: '1rem', marginBottom: 8, height: 120 }}>
        <div style={{ display: 'table-row' }}>
          <div style={{ display: 'table-cell', verticalAlign: 'middle', width: '10%' }}>
            <img src={`${thumbnail}`} style={{ width: 90, height: 90 }} alt={title} />
          </div>
          <div style={{ display: 'table-cell', verticalAlign: 'middle', width: '80%', padding: 10 }}>
            <div className="title" style={{ width: '90%', paddingBottom: '0.5rem', paddingLeft: '0.5rem' }}>{title}</div>
            <div className="data-row description" style={{ width: '90%', paddingLeft: '0.5rem' }}>{body}</div>
          </div>
        </div>
      </div>
    );
  }
}

export default GoLive;