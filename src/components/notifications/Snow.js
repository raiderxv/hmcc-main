import React, { Component } from 'react';

class Snow extends Component {
  render() {
    const { SLA_Status, Severity, Title, Incident_Start_Date_GMT, IncidentNumber } = this.props.data;
    // const color = SLA_Status.toLowerCase().includes('exceeded') ? '241,99,82' : '255,146,0';
    const color = Severity === "P1" ? '241,99,82' : '255,146,0';
    return (
      <div className="snow">
        <div style={{ backgroundColor: `rgba(${color})` }}>
        </div>
        <div style={{ backgroundColor: `rgba(${color}, 0.5)` }}>
          <div className="data-row"><span>{Severity}</span>&nbsp; - &nbsp;<span>{IncidentNumber}</span></div>
          <div className="title" >{Title}</div>
          <div className="data-row"><span>Date:&nbsp;</span><span>{Incident_Start_Date_GMT}</span></div>
        </div>
      </div>
    );
  }
}

export default Snow;