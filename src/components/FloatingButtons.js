import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import Draggable from 'react-draggable'
import { Container, Button } from 'react-floating-action-button'

import './FloatingButtons.css'

export default class FloatingButtons extends Component {
  constructor(props) {
    super(props)

    this.state = {}
  }


  componentWillMount() {
    const state = this.props

    this.setState(state)
  }

  componentDidUpdate(prevProps, prevState) {
    const state = this.props
    if (state == prevProps) return

    document.getElementsByClassName('sys-draggable')[0].setAttribute("style", "")
    document.getElementsByClassName('sys-draggable')[0].setAttribute("style", `transform: translate(${state.floatingButtonPosition.x}px, ${state.floatingButtonPosition.y}px)`)

    this.setState(state)
  }

  triggerWindowFunctions = (functionKey) => {
    try {
      switch (functionKey) {
        case 1:
          window.exitApplication()
          break;

        case 2:
          window.minimizeApp()
          break;
      
        case 3:
          window.refreshApp()
          break;

        case 4:
          window.reloadDashboards()
          break;

        case 5:
          window.toggleDrawer()
          break;

        default:
          break;
      }
      
    } catch (e) {
      console.log(e)
    }
  }

  render() {
    const { fabDisplay, imgSrc, floatingButtonPosition } = this.state

    return (
      <Draggable
        axis="both"
        handle=".handle"
        positionOffset={floatingButtonPosition}
        grid={[0, 0]}
        scale={1}
        onStart={this.handleStart}
        onDrag={this.handleDrag}
        onStop={this.handleStop}>
        <div className={`sys-draggable ${fabDisplay}`}
          onMouseLeave={() => {this.props.changeFabDisplayStatusWithTimer()}}
          onMouseEnter={() => { this.props.changeFabDisplayStatusMouseEnter() }}
          >
          <div className="handle">
            <Container className="sys-fab-container">
              <Button
                className="fab-button fab-exit"
                tooltip="Exit Application"
                icon="fa fa-times"
                onClick={() => this.triggerWindowFunctions(1)} />
              <Button
                className="fab-button  fab-minimize"
                tooltip="Minimize Application"
                icon="fa fa-minus"
                onClick={() => this.triggerWindowFunctions(2)} />
              <Button
                className="fab-button  fab-refresh"
                tooltip="Refresh Application"
                rotate={true}
                icon="fa fa-refresh"
                onClick={() => this.triggerWindowFunctions(3)} />
              <Button
                className="fab-button fab-refresh-all"
                tooltip="Refresh All Dashboards"
                icon="fa fa-bar-chart"
                onClick={() => this.triggerWindowFunctions(4)} >
                <i className="fa fa-refresh dashboard-refresh-icon" />
              </Button>
              <Button
                className="fab-button fab-clients"
                tooltip="Toggle Drawer"
                // icon="fal fa-globe"
                onClick={() => this.triggerWindowFunctions(5)} >
                <img src="ui-assets/images/icons/globe.png" className="fab-item-globe" />
              </Button>
              <Button
                className="fab-button-main"
                onClick={() => this.props.changeFabDisplayStatus()} >
                <img src={imgSrc} className="fab-item-image" />
              </Button>
            </Container>
          </div>
        </div>
      </Draggable>
    )
  }
}