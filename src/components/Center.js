import React, { Component } from "react";
import Switch from 'react-switch';
import CountUp from 'react-countup';
import { geoCircle, geoPath, geoNaturalEarth1, geoIdentity } from 'd3-geo';
import Clock from 'react-live-clock';

// import Deployments from './data/deployments.json';

import mapStyles from '../styles/map.js';

import {
  ComposableMap,
  ZoomableGroup,
  Geographies,
  Geography,
  Markers,
  Marker
} from "react-simple-maps";
import { Motion, spring } from "react-motion";

import '../styles/center/map.css';
import '../styles/center/filter.css';
import '../styles/center/mapping.css';
import './Center.css'

// import SVGMap from "./SVGMap";
const solar = require('solar-calculator');
const regionConfig = {
  r: 40,
  x: 50,
  y: 40
}
const wrapperStyles = {
  backgroundColor: 'rgba(255,255,255,0)',
  maxWidth: 1920,
  margin: '0 auto',
  borderRadius: '40%'
  // backgroundImage: `url(${process.env.PUBLIC_URL}/ui-assets/images/globe_background.png)`,
  // backgroundSize: 'cover'
}

const switchStyles = {
  on: { fontFamily: 'Graphik Regular', padding: '6px 10px', fontSize: '16pt' },
  off: { fontFamily: 'Graphik Regular', padding: '6px 5px', fontSize: '16pt' }
}


const operationStyles = {
  clientServedLabel: {
    display: 'table-cell',
    textAlign: 'center',
    fontSize: '1.7rem',
    verticalAlign: 'middle',
    padding: '0 26px',
    fontFamily: 'Graphik Regular'
  },
  clientServedValue: {
    display: 'table-cell',
    textAlign: 'center',
    fontSize: '1.7rem',
    verticalAlign: 'middle',
    borderBottomLeftRadius: '10px',
    borderBottomRightRadius: '10px',
    fontFamily: 'Graphik Regular'
  },
  title: {
    fontSize: '2rem',
    padding: '0.5rem 0rem'
  }
}

const countryConfig = {
  default: {
    fill: "#0E4FE5",
    stroke: "#0E4FE5",
    strokeWidth: 1,
    outline: "none",
  },
  hover: {
    fill: "#0E4FE5",
    stroke: "0E4FE5",
    strokeWidth: 1,
    outline: "none",
  },
  pressed: {
    fill: "0E4FE5",
    stroke: "0E4FE5",
    strokeWidth: 1,
    outline: "none",
  },
}

class AnimatedMap extends Component {
  constructor(props) {
    super(props);
    let humanCount = { old: 0, new: 0 }, machineCount = { old: 0, new: 0 };
    this.state = {
      center: [0, 20],
      zoom: 1.05,
      cityName: '',
      humanCount: humanCount,
      machineCount: machineCount,
      humanOn: false,
      machineOn: false,
      filterActive: false,
      fetchingData: false,
      firstLoad: true,
      countryMap: 'world',
      machinHumanCountContainerPos: { left: 0 },
      filtersContainerPos: { left: 0 },
      circleLines: [],
      times: [],
      nightTime: '',
      showDayNight: true,
      activeFilterId: 0,
      activeFilters: {},
      originalFilters: {},
      regionDeployments: {},
      deployments: this.props.deployments,
      clientName: null
    }
    this.handleCityClick = this.handleCityClick.bind(this)
    this.handleReset = this.handleReset.bind(this);
    this.projection = this.projection.bind(this);
    this.dayNightInterval = false;
    this.dayNightCityInterval = false;
    this.geoClockInterval = false;
    this.arrayColorCount = 16;
    this.deploymentsInterval = false;

    
    // console.log(Deployments)
  }

  dayAndNightAnimation() {
    this.setState({ daynight: window.calculateDayNight() }); // 
    if (!this.dayNightInterval)
      this.dayNightInterval = setInterval(() => this.dayAndNightAnimation(), 30000)
  }

  fetchDeployments() {
    this.setState({ fetchingData: true }) // fetching data
    // fetch data from server
    fetch(`${this.props.config.fetchAfterPageLoad.deployments}?token=${this.props.config.token}&v=${(new Date()).getTime()}`).then(data => data.json()).then(data => {
      let countryKeys = {};
      let humanCount = { old: 0, new: 0 }, machineCount = { old: 0, new: 0 }; // set the human count and region count per region
      const datus = data;

      const filters = [];
      const subFilterActive = {};
      datus.filters.map(f => {
        const key = f.name.replace(/\s/g, '');
        subFilterActive[key] = f.types;
        filters.push({
          id: key,
          name: f.name,
          items: f.types
        });
      })
      this.setState({ deployments: data.data, fetchingData: false, filters, subFilterActive, ...countryKeys, humanCount, machineCount }, () => {
        if (!this.deploymentsInterval)
          this.deploymentsInterval = setInterval(() => this.fetchDeployments(), 900000);
      })
    })
  }

  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  async showLoading() {

    if(!this.state.firstLoad) {
      this.setState({fetchingData: true});
  
      await this.sleep(10000).then(()=>{
        this.setState({fetchingData: false});
      });
    } else{
      this.setState({firstLoad: false});
    }

    
    setTimeout(() => this.showLoading(), 900000);
  }


  componentDidMount() {
    const parentNode = document.getElementById('maps-container').getBoundingClientRect();
    const self = this

    this.setState({ machinHumanCountContainerPos: { left: parentNode.x + 40 }, filtersContainerPos: { left: (parentNode.x + parentNode.width) } });
    this.setState({ nightTime: this.night() }, () => {
      this.dayNightInterval = setInterval(() => this.setState({ nightTime: this.night() }), 60000)
    })
    
    // this.computeTotalHumanAndMachine();
    this.setDeploymentsData();
    this.initializeActiveFilters();
    this.initializeRegionDeployments();
    this.inputSubFilterClick.click();
    this.inputSubFilterClick.click();
    this.showLoading();

    window.clientName = (clientName = null) => self.setState({ clientName: clientName.toUpperCase() }, () => this.checkClientName(clientName))
          
    //this.fetchDeployments();

  }

  checkClientName = (clientName) => {
    return clientName.toUpperCase() === 'ALL' ?
      setTimeout(() => this.setState({ clientName: null }), 5000) : false
  }

  // componentWillReceiveProps(nextProps) {
  //   console.log('componentWillReceiveProps')
  //   console.log(nextProps.deployments)
  //   this.setDeploymentsData(nextProps.deployments)
  //   this.computeDeploymentsByRegion();
  // }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if(prevProps.deployments != this.props.deployments) {
      this.setDeploymentsData()
      this.initializeActiveFilters();
      this.initializeRegionDeployments();
      this.computeDeploymentsByRegion();
      this.resetSubFilterClick()
    }
  }

  componentWillUnmount() {
    clearInterval(this.dayNightInterval)
  }

  setDeploymentsData() {
    let humanCount = { old: 0, new: 0 }, machineCount = { old: 0, new: 0 };

    this.setState({deployments: this.props.deployments, humanCount: humanCount, machineCount: machineCount});
  }

  computeTotalHumanAndMachine() {
    let humanCount = {new: 0, old: 0};
    let machineCount = {new: 0, old: 0};

    Object.keys(this.props.data).map( region => {
      humanCount.new+=this.props.data[region].human;
      machineCount.new+=this.props.data[region].machine;
    });
    
    this.setState({humanCount, machineCount});
  }

  computeDeploymentsByRegion() {
    const {activeFilters, regionDeployments} = this.state;
    const {deployments} = this.props;

    if(Object.keys(deployments['human']).length && Object.keys(deployments['machine']).length) {
      ["human", "machine"].forEach(type => {

        Object.keys(deployments[type]).map( region => {
          let sum = 0;
          const activeRecords = deployments[type][region].filter(record => {
            return (
              activeFilters.AssetType.includes(record.AssetType) && 
              activeFilters.DeliveryLocation.includes(record.DeliveryLocation) && 
              activeFilters.IndustryGroup.includes(record.OperatingGroup) && 
              activeFilters.Offering.includes(record.Offering)
            );
          });

          if(activeRecords.length > 0) {
            sum = activeRecords.map(r=>r.DataCount).reduce((current, next) => current + next);
          }
          regionDeployments[region][type].new = sum;
        });
      });

      this.setState({ regionDeployments });
      this.computeTotalHumanAndMachineByFilter();
    } else {
      this.setState({ 'regionDeployments' : {} });
    }
  }

  computeTotalHumanAndMachineByFilter() {
    const {regionDeployments, humanCount, machineCount} = this.state;
    humanCount.new = 0;
    machineCount.new = 0;
    Object.keys(regionDeployments).map(r => {
      humanCount.new+=regionDeployments[r].human.new;
      machineCount.new+=regionDeployments[r].machine.new;
    });
    this.setState({humanCount, machineCount});
  }

  initializeRegionDeployments() {
    const {regionDeployments} = this.state;
    const {data} = this.props;
    Object.keys(data).map(region => {
      regionDeployments[region] = {human: {old: 0, new: data[region].human}, machine: {old: 0, new: data[region].machine}};
    });
    this.setState({regionDeployments});
  }

  initializeActiveFilters() {
    const {filters} = this.props;
    const {activeFilters} = this.state;
    activeFilters["original"]={};
    Object.keys(filters).map(key => {
      activeFilters[key] = [];
      
      activeFilters["original"][key] = []
      filters[key].items.forEach(f => {
        activeFilters[key].push(f.id);
        activeFilters["original"][key].push(f.id);
      });
    });
    this.setState({activeFilters});
  }
  // const opacity = window.SunCalc.getPosition(new Date(), city.Latitude, city.Longitude).altitude > 0 ? 0 : 1;
  // handle zooming of the region 
  handleCityClick(city) {
    if (this.state.cityName === city.name || city.name.toLowerCase() === "other regions") {
      this.handleReset();
      return;
    }
    this.setState({
      zoom: 6,
      center: city.coordinates,
      cityName: city.name
    })
  }
  handleReset() {
    this.setState({
      center: [0, 20],
      zoom: 1.05,
      cityName: ''
    })
  }
  //  filters
  handleFilterClick = (e) => {
    const { data, filters } = this.props;
    const target = e.target;
    const filterActive = target.dataset.id;
    const activeFilterId = target.dataset.id;

    if (activeFilterId === this.state.activeFilterId) {
      this.setState({ activeFilterId: 0 })
      return;
    }
    const rect = target.getBoundingClientRect();
    // const subFilters = filters[target.dataset.id].items; // get the subFilters of filter clicked
    // console.log(subFilters);
    // put the position of container when filter is click base of height
    this.setState({ filterActive, activeFilterId, subFilterPosition: { left: rect.left - 470, width: rect.width, top: rect.top } })
  }

  resetSubFilterClick = () => {
    var el_class = document.querySelectorAll(".filter-class");
    var el_checkbox = document.querySelectorAll(".filter-checkbox");

    [].forEach.call(el_class, function (el) {
      el.classList.add("active")
    });

    [].forEach.call(el_checkbox, function (el) {
      el.style.display = "block"
    });
  }

  handleSubFilterClick = (e) => {
    // console.log('handleSubFilterClick')

    const {activeFilters} = this.state;
    const el = e.currentTarget;

    const filterKey = el.dataset.filterkey;
    const filterId = el.dataset.id;
    const filterIndex = activeFilters[filterKey].indexOf(filterId);

    const setFilterState = (f, activate)=>{
      if(activate) {
        f.classList.add("active");
        f.children[1].children[0].style.display = "block"
      } else{
        f.classList.remove("active");
        f.children[1].children[0].style.display = "none"
      }
      
    }
    
    if(el.classList.contains("active")) {
      if(filterId == "all"){
        activeFilters[filterKey] = ["all"];
        const filters = el.parentNode.children;
        for(var i=0; i < filters.length; i++) {
          setFilterState(filters[i], false);
        }
      }else{
        activeFilters[filterKey].splice(filterIndex,  1);
        setFilterState(el, false);
        setFilterState(el.parentNode.children[0], activeFilters[filterKey].length === activeFilters["original"][filterKey].length);
      }
      
    }else{
      if(filterId == "all") {
        activeFilters[filterKey] = activeFilters["original"][filterKey].map(i => i);
        const filters = el.parentNode.children;
        for(var i=0; i < filters.length; i++) {
          setFilterState(filters[i], true);
        }
      }else {
        activeFilters[filterKey].push(filterId);
        setFilterState(el, true);
        setFilterState(el.parentNode.children[0], activeFilters[filterKey].length === activeFilters["original"][filterKey].length);
      }
    }

    this.setState({activeFilters});

    this.computeDeploymentsByRegion();
    this.computeTotalHumanAndMachineByFilter();
  }
  // set the switch of human and machine
  handleSwitch(node) {
    const { machineCount, humanCount } = this.state;
    // this.handleSubFilterClick();
    this.setState({ [node]: !this.state[node] })
  }


  projection(width, height, config) {
    return geoNaturalEarth1()
      .rotate([0, 0, 0])
      .translate([960, 540])
      .scale(400)
  }

  // calculate the day and night
  sun = () => {
    const now = new Date();
    const day = new Date(+now).setUTCHours(0, 0, 0, 0);
    const t = solar.century(now);
    const longitude = (day - now) / 864e5 * 360 - 180;
    return [longitude - solar.equationOfTime(t) / 4, solar.declination(t)];
  }
  // use in calculation for day and night
  antipode = ([longitude, latitude]) => [longitude + 180, -latitude]
  // return all calculated shade
  night = () => {
    const path = geoPath(this.projection())
    return path(geoCircle().radius(90).center(this.antipode(this.sun()))());
  }

  render() {
    const { clientServed, filters } = this.props;
    const { humanCount, machineCount, regionDeployments, activeFilters, activeFilterId, machineOn, humanOn, filtersContainerPos, nightTime, fetchingData = true, showDayNight, clientName } = this.state;
    const filterStyle = { width: '100%', fontFamily: "Graphik Regular", fontSize: '1.4rem', padding: '1rem 0rem 1rem 2rem', borderBottom: '2px solid rgba(0, 0, 0, 0.5)', position: 'relative' }
    
    return (
      <div className="command-center-panel maps col-sm-1 col-4" id="maps-container" style={{ display: `${this.props.operationView ? 'none' : 'inline-flex'}`, ...wrapperStyles }} onClick={() => this.props.hideFabDisplayStatus()} >
        <Motion
          defaultStyle={{
            zoom: 1.05,
            x: 0,
            y: 20,
          }}
          style={{
            zoom: spring(this.state.zoom, { stiffness: 210, damping: 30 }),
            x: spring(this.state.center[0], { stiffness: 210, damping: 20 }),
            y: spring(this.state.center[1], { stiffness: 210, damping: 20 }),
          }}
        >
          {({ zoom, x, y }) => (
            <ComposableMap
              projection={this.projection}
              projectionConfig={{ scale: 400 }} // scale of map
              width={1920}
              height={1080}
              style={{
                position: 'absolute',
                height: 1800,
                marginTop: -357,
                borderRadius: '47%'
              }}
            >
              <ZoomableGroup center={[x, y]} zoom={zoom} disablePanning={true}>
                {/* <Graticule stroke="rgba(0,0,0,0.5" /> */}
                {/* draw the mao */}
                <Geographies geography={`${process.env.PUBLIC_URL}/ui-assets/maps/world-50m.json`}>
                  {(geographies, projection) =>
                    geographies.map((geography, i) => {
                      return geography.id !== "010" && (
                        <Geography
                          key={i}
                          geography={geography}
                          projection={projection}
                          style={{
                            default: {
                              fill: "rgb(0,77, 255)",
                              stroke: "rgb(0,77, 255)",
                              strokeWidth: 1,
                              outline: "none",
                            },
                            hover: {
                              fill: "rgb(0, 77, 255)",
                              stroke: "rgb(0, 77, 255)",
                              strokeWidth: 1,
                              outline: "none",
                            },
                            pressed: {
                              fill: "rgb(0, 77, 255)",
                              stroke: "rgb(0, 77, 255)",
                              strokeWidth: 1,
                              outline: "none",
                            }
                          }}
                        />
                      )
                    })
                  }
                </Geographies>
                {showDayNight && <g>
                  <path d={nightTime} color="#fff" fill="rgba(0,0,0,0.5)" />
                </g>}

                <Markers>
                  {this.props.clientGeo.data.map((client, i) => {
                    const geoID = client.Region_Name.replace(/\s|\//g, '');
                  
                    const geo = { name: client.Region_Name, coordinates: [client.Longitude, client.Latitude] };
                    const countDefault = { old: 0, new: 0 };
                    
                    const humanRegionCount = humanOn ? countDefault : regionDeployments[geoID] ? regionDeployments[geoID].human : countDefault;
                    const machineRegionCount = machineOn ? countDefault : regionDeployments[geoID] ? regionDeployments[geoID].machine : countDefault;
                    return <Marker
                      key={i}
                      marker={geo}
                      onClick={this.handleCityClick}
                      outline="none"
                    >
                      <circle
                        cx={0}
                        cy={0}
                        r={5}
                        fill="#FFF"
                        stroke="#fff"
                        strokeWidth="5"
                      />
                      <circle
                        cx={-regionConfig.x}
                        cy={-regionConfig.y}
                        r={regionConfig.r}
                        fill={humanOn ? "#969696" : "rgb(0,77,255)"}
                        stroke="#fff"
                        strokeWidth="5"
                      />
                      <circle
                        cx={-regionConfig.x}
                        cy={-regionConfig.y}
                        r={regionConfig.r}
                        fill={humanOn ? "#969696" : "rgb(0,77,255)"}
                        stroke="#FFF"
                        strokeWidth="5"
                        className={fetchingData ? 'loading' : 'unloading'}// loading when fetching data
                      />
                      {/* <text x={-regionConfig.x} y={-regionConfig.y + 5} textAnchor="middle" stroke={machineOn ? "#969696" : "#00BAFF"} strokeWidth="2px" fontSize="110%">{this.state[geoID] ? this.state[geoID].machines.new : 0}</text> */}

                      <foreignObject textAnchor="middle" x={-((regionConfig.r * 2) + 10)} y={-(regionConfig.y + 9)} style={{ fontWeight: 'bolder', color: "#fff", fontFamily: 'Graphik Regular', textAlign: 'center', fontSize: '1rem', height: 25, width: (regionConfig.r * 2) }}>
                        <CountUp start={humanRegionCount.old} end={humanRegionCount.new} duration={0.5} />
                      </foreignObject>
                      {/* white circle background */}
                      <circle
                        cx={regionConfig.x}
                        cy={-regionConfig.y}
                        r={regionConfig.r}
                        fill="#FFF"
                        stroke="#fff"
                        strokeWidth="5"
                      />
                      {/* second coating for the circle */}
                      <circle
                        cx={regionConfig.x}
                        cy={-regionConfig.y}
                        r={regionConfig.r}
                        fill="#FFF"
                        stroke={machineOn ? "#969696" : "#00BAFF"}
                        strokeWidth="5"
                        className={fetchingData ? 'loading' : 'unloading'} // loading when fetching data
                      />
                      {/* count per region */}
                      <foreignObject textAnchor="middle" x={10} y={-(regionConfig.y + 9)} style={{ fontWeight: 'bolder', color: (machineOn ? "#969696" : "#00BAFF"), fontFamily: 'Graphik Regular', textAlign: 'center', fontSize: '1rem', height: 25, width: (regionConfig.r * 2) }}>
                        <CountUp start={machineRegionCount.old} end={machineRegionCount.new} duration={0.5} />
                      </foreignObject>
                      {/* name of the region */}
                      <text x="0" y={regionConfig.y - 5} textAnchor="middle" fill="#FFF" fontSize="22px">{client.Region_Name.toUpperCase()}</text>
                      {/* time for the region */}
                      {client.TimeZone && <foreignObject x={-90} y={40} style={{ fontFamily: 'Graphik Regular', textAlign: 'center', fontSize: '20px', color: '#FFF', height: 40, width: 180 }}>
                        <Clock format={"hh:mm:ss A"} ticking={true} timezone={client.TimeZone} />
                      </foreignObject>}
                    </Marker>
                  })}
                </Markers>
              </ZoomableGroup>
            </ComposableMap>
          )}
        </Motion>
        <div className="filters" style={filtersContainerPos}>
          {filters && Object.keys(filters).map(filter_key => {
 
            const filter = filters[filter_key];

            return <div key={filter_key} className={`item${activeFilterId === filter.id ? ' active' : ''} `}>
              <div
                data-id={filter.id}
                onClick={this.handleFilterClick}
                style={{ backgroundImage: `url(${process.env.PUBLIC_URL}/ui-assets/images/buttons/filters/${filter_key.toUpperCase()}_ON.png)` }}
              // ${machineOn ? 'OFF' : 'ON'}
              >
                {filter.description} 
                <i style={{fontSize: "15px", textTransform: "capitalize"}}>&nbsp; 
                  {activeFilters && activeFilters[filter_key] && activeFilters[filter_key].length === activeFilters["original"][filter_key].length ? "" :  "(" + (activeFilters[filter_key] ? activeFilters[filter_key].length - 1 : "") + ")" }
                </i>
              </div>
              <div className="subfilter-dropdown" style={activeFilterId == filter.id ? {} : {display: 'none'}}>
                {filter.items.map(filter=> {
                  return <div key={filter.id} className={`filter-class item active`} style={filterStyle} data-filterkey={filter_key} data-id={filter.id} onClick={this.handleSubFilterClick.bind(this)} ref={input => this.inputSubFilterClick = input}>
                    <div style={{width: '80%', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap', fontSize: '1.2rem'}}>{filter.desc}</div>
                    <div className="filter-checkbox-container">
                      <div className="filter-checkbox"></div>
                    </div>
                  </div>
                })}
              </div>
            </div>
          })}
        </div>
        <div style={{ clear: 'both' }}></div>
        {clientName && <div className="client-container">
          <div className="label-container">
            <img src={`${process.env.PUBLIC_URL}/ui-assets/images/operations/client-serves.png`} className="client-icon" />
            <span className="ml-2">CLIENT SERVED: </span>
            <span className="client-name ml-2">{clientName}</span>
          </div>
        </div>}
        <div className="client-served" style={{
          width: 1920,
          position: "absolute",
          top: 0,
          left: '50%',
          transform: 'translateX(-50%)',
          backgroundColor: 'rgba(0, 186, 255, 0.2)',
          borderBottomLeftRadius: '200%',
          borderBottomRightRadius: '200%',
          height: 88

        }}
        >
          <div style={{ position: "relative", top: 0, left: '50%', transform: 'translateX(-50%)', width: 'max-content', display: 'inline-flex', paddingTop: 10 }}>


            <div className="label" style={{
              display: 'inline-flex',
              fontSize: '1.5rem',
              width: 'max-content',
              padding: '0.2rem 90px'
            }}>
              <img src={`${process.env.PUBLIC_URL}/ui-assets/images/operations/client-serves.png`} style={{ width: 20, height: 26, marginTop: 2 }}></img>
              &nbsp;&nbsp;CLIENTS SERVED
                </div>
            <div style={{ display: 'table' }}>
              <div style={{ display: 'table-row' }}>
                <div style={{ ...operationStyles.clientServedLabel, color: '#00BAFF', fontSize: 20 }}>OVERALL</div>
                {clientServed.data.map((client, index) => {
                  return <div key={index} style={{ ...operationStyles.clientServedLabel, fontSize: 20, color: '#00BAFF' }}>{client.ClientGeo.toUpperCase()}</div>
                })}
              </div>
              <div style={{ display: 'table-row', fontFamily: 'Graphik' }}>
                <div style={{ ...operationStyles.clientServedValue, color: '#FFF', fontSize: 30 }}>{clientServed.data.reduce((a, b) => a + b.ClientCount, 0)}</div>
                {clientServed.data.map((client, index) => {
                  return <div key={index} style={{ ...operationStyles.clientServedValue, color: '#FFF', fontSize: 30 }}>{client.ClientCount}</div>
                })}
              </div>
            </div>
          </div>
        </div>

        <div className="client-served-bottom" style={{
          width: 2147,
          position: 'absolute',
          bottom: 0,
          height: 180,
          left: '49.94%',
          transform: 'translateX(-50%)',
          backgroundColor: 'rgba(0, 186, 255, 0.2)',
          borderTopLeftRadius: '200%',
          borderTopRightRadius: '200%'

        }}
        >
          <div style={mapStyles.totalHumaMachineCount}>
            <div style={mapStyles.first}>HUMAN + MACHINE</div>
            <div style={{ ...mapStyles.second, marginTop: -10 }}>
              <div className="label-count" style={{ display: 'table-row' }}>
                <div style={{ ...mapStyles.tableCell, width: '38.9%', fontSize: '1.5rem' }}>
                  <Clock format={"MMMM DD, YYYY hh:mm:ss A"} ticking={true} timestamp={Intl.DateTimeFormat().resolvedOptions().timeZone} style={{ marginLeft: 5, marginTop: -40 }} />
                </div>
                <div style={{ ...mapStyles.tableCell, paddingRight: 20, width: '5%' }}>
                  <Switch onChange={() => this.handleSwitch('humanOn')}
                    checked={humanOn}
                    offColor="#004dff"
                    onColor="#FFF"
                    onHandleColor="#969696"
                    offHandleColor="#fff"
                    width={100}
                    height={40}
                    handleDiameter={40}
                    checkedIcon={<div style={{ ...switchStyles.on, color: '#969696' }}>OFF</div>}
                    uncheckedIcon={<div style={switchStyles.off} > ON</div>}
                  />
                </div>
                <div className={`numbers ${humanOn ? 'disabled' : ''}`} style={{ ...mapStyles.tableCell, width: '4.5%' }}>
                  <div style={{ fontFamily: 'Graphik Regular' }}>
                    <div><CountUp start={humanCount.old} end={humanOn ? 0 : humanCount.new} duration={0.5} /></div>
                  </div>
                </div>
                <div className={`numbers ${machineOn ? 'disabled' : ''}`} style={{ ...mapStyles.tableCell, width: '4.5%' }}>
                  <div style={{ fontFamily: 'Graphik Regular' }}>
                    <div><CountUp start={machineCount.old} end={machineOn ? 0 : machineCount.new} duration={0.5} /></div>
                  </div>
                </div>
                <div style={{ ...mapStyles.tableCell, paddingLeft: 20, width: '5%' }} >
                  <Switch onChange={() => this.handleSwitch('machineOn')}
                    checked={machineOn}
                    offColor="#00BAFF"
                    onColor="#969696"
                    width={100}
                    height={40}
                    handleDiameter={40}
                    checkedIcon={<div style={switchStyles.on}>OFF</div>}
                    uncheckedIcon={<div style={switchStyles.off}>ON</div>}
                  />
                </div>

                <div style={{ ...mapStyles.tableCell, width: '40%' }}>
                  <img src={`${process.env.PUBLIC_URL}/ui-assets/images/accenture-logo.png`} style={{ height: '15%', marginLeft: 20, marginTop: -25 }} />
                </div>

              </div>
            </div>
          </div>
        </div>
        {/* {subFilters && <div className="sub-filter" style={{ ...subFilterPosition }}>
          {subFilters.map(filter => {
            return <div key={filter} className={`item${subFilterActive[filterActive].includes(filter) ? ' active' : ''}`} data-id={filter} onClick={this.handleSubFilterClick}>{filter}</div>
          })}
        </div>} */}
        <button id="show-daynight-button" style={{ display: 'none' }} onClick={() => this.setState({ showDayNight: true })} ></button>
        <button id="hide-daynight-button" style={{ display: 'none' }} onClick={() => this.setState({ showDayNight: false })} ></button>
      </div >
    )
  }
}

export default AnimatedMap